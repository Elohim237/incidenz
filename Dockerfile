# See here for image contents: https://github.com/microsoft/vscode-dev-containers/tree/v0.231.6/containers/javascript-node/.devcontainer/base.Dockerfile
# [Choice] Node.js version (use -bullseye variants on local arm64/Apple Silicon): 16, 14, 12, 16-bullseye, 14-bullseye, 12-bullseye, 16-buster, 14-buster, 12-buster
FROM node:18.4.0-bullseye-slim

WORKDIR /workspace
COPY . .

# OS packages
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends python git

RUN npm install -g eslint
RUN npm install -g nodemon
RUN rm -rf node_modules
RUN npm install


EXPOSE 4002
CMD [ "nodemon", "bin/www" ]

