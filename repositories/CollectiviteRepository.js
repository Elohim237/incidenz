require('dotenv').config();

const Collectivite = require('../models/Collectivite');

class CollectiviteRepository {

    async getAllWithPagination(options){
        try {
            const regex = new RegExp(options.search, 'i');
            const agregate = Collectivite.aggregate([
                {
                    $sort: {
                        [options.sortBy]: options.sortDirection,
                    },
                },
                {

                    $match: {
                        $and: [
                            {
                                $or: [
                                    { name: regex },
                                    { adresse: regex },
                                    { email: regex },
                                    { tel: regex },
                                ],
                            },
                        ],
                    },
                },

            ])
            return await Collectivite.aggregatePaginate(agregate, options)
        } catch (error) {
            throw error;
        }
    }

    async getAll(){
        try {
            return await Collectivite.find()
        } catch (error) {
            throw error;
        }
    }

    async save(data){
        try {
            return await Collectivite.create(data)
        } catch (error) {
            throw error;
        }
    }

    async update(data, id) {
        try {
            return await Collectivite.updateOne({_id: id}, data)
        } catch (error) {
            throw error;
        }
    }


}

const collectivity = new CollectiviteRepository();
module.exports = collectivity;