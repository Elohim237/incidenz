require('dotenv').config();

const responsable = require('../models/Responsable');

class ResponsableRepository {

    async save(data) {
        try {
            return await responsable.create(data)
        } catch (error) {
            throw error;
        }
    }

}

const responsableRepository = new ResponsableRepository();
module.exports = responsableRepository;