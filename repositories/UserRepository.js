require('dotenv').config();

const user = require('../models/User');

class UserRository {

    async save(data) {
        try {
            return await user.create(data)
        } catch (error) {
            throw error;
        }
    }

}

const userRepository = new UserRository();
module.exports = userRepository