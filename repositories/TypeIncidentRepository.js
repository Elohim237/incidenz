
// CREATE (add a new incident type)
const TypeIncident = require('../models/TypeIncident');
const category = require('../models/Categorie');
const responsable = require('../models/Responsable');
const {disconnect, Types} = require("mongoose");
const Collectivite = require("../models/Collectivite");

class TypeIncidentRepository {

    async getAllWithPagination(typeIncidentData) {
        try {
            const regex = new RegExp(typeIncidentData.search, 'i');

            const aggregate = TypeIncident.aggregate([
                {
                    $sort: {
                        [typeIncidentData.sortBy]: typeIncidentData.sortDirection,
                    },
                },
                {
                    $match: {
                        $or: [
                            { name: regex },
                            { category: regex }, // Assurez-vous que c'est 'category' ou 'categoryId'
                        ],
                    },
                },
                {
                    $lookup: {
                        from: 'categories', // Assurez-vous que c'est le bon nom de votre collection de catégories
                        localField: 'categoryId', // Assurez-vous que c'est le bon nom de votre champ de catégorie dans TypeIncident
                        foreignField: '_id', // Assurez-vous que c'est le bon nom de votre champ d'ID dans la collection Categorie
                        as: 'categoryInfo',
                    },
                },
            ]);

            const result = await TypeIncident.aggregatePaginate(aggregate, typeIncidentData);

            return result;
        } catch (error) {
            throw error;
        }
    }


// CREATE (add a new incident type)
    async createTypeIncident(typeIncidentData){
        try {

            // Check if the type already exists
            const existingType = await typeIncident.findOne({ name: typeIncidentData.name });
            if (existingType) {
                throw new Error('Type already exists');
            }

            // Check if categoryId exists
            const existingCategory = await category.findById(typeIncidentData.categoryId);
            if (!existingCategory) {
                throw new Error('Category not found');
            }

            // Check if responsableId exists
            const existingResponsable = await responsable.findById(typeIncidentData.responsableId);
            if (!existingResponsable) {
                throw new Error('Responsable not found');
            }

            return await typeIncident.create(typeIncidentData);

        } catch (error) {
            throw error;
        }
    };

// READ (get all incident types)
    async getAllTypeIncidents() {
        try {
            return await TypeIncident.find().populate('categoryId');
        } catch (error) {
            throw error;
        }
    };

// UPDATE (update an incident type)
    async updateTypeIncident(typeIncidentId, updatedData) {
        try {
            // Convert the provided ID to ObjectId
            const objectId = Types.ObjectId(typeIncidentId);

            // Check if the type exists
            const existingType = await TypeIncident.findById(objectId);

            if (!existingType) {
                throw new Error('Type not found');
            }

            // Check if categoryId exists
            if (updatedData.categoryId) {
                const existingCategory = await category.findById(updatedData.categoryId);
                if (!existingCategory) {
                    throw new Error('Updated category not found');
                }
            }

            // Check if responsableId exists
            if (updatedData.responsableId) {
                const existingResponsable = await responsable.findById(updatedData.responsableId);
                if (!existingResponsable) {
                    throw new Error('Updated responsable not found');
                }
            }

            return await typeIncident.findByIdAndUpdate(
                objectId,
                updatedData,
                { new: true }
            );

        } catch (error) {
            throw error;
        }


    };

// DELETE (delete an incident type)
    async deleteTypeIncident (typeIncidentId) {
        try {

            // Convert the provided ID to ObjectId
            const objectId = Types.ObjectId(typeIncidentId);

            // Check if the type exists
            const existingType = await TypeIncident.findById(objectId);
            if (!existingType) {
                throw new Error('Type not found');
            }

            return await TypeIncident.findByIdAndRemove(objectId);

        } catch (error) {
            throw error;
        }

    };


//GET BY categoryId
    async getByCategoryId (categoryId){
        try {
            const existingCategory = await category.findById(categoryId);
            if (!existingCategory)
                throw new Error('category does not exist')
            const result = await typeIncident.find({categoryId: categoryId});
            return result;
        } catch (error) {
            throw error;
        }
    }

}







const typeIncident = new TypeIncidentRepository();

module.exports = typeIncident;
