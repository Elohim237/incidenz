require('dotenv').config();

const Incident = require('../models/Incident');
const Usager = require('../models/Usager')

class IncidentRepository {
    async save(data, id) {

        try {
            if (id === 'NONE') {
                const newUsager = await Usager.create({username: "NONE"});
                id = newUsager.id;
            }
            data.usagerId = id;
            return await Incident.create(data);
        } catch (err) {
            throw err;
        }

    }
}

const incidentRepository =  new IncidentRepository();

module.exports = incidentRepository;