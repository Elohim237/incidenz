require('dotenv').config();

const category = require('../models/Categorie');

class CategoryRository {

    async save(data) {
        try {
           return await category.create(data)
        } catch (error) {
            throw error;
        }
    }

    async list() {
        try {
            return await category.find();
        } catch (error) {
            throw error;
        }
    }

}

const categoryRepository = new CategoryRository();

module.exports = categoryRepository;