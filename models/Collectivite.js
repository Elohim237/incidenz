const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const collectiviteSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    tel: {
        type: String,
        required: true,
    },
    address: {
        type: String
    }
}, { timestamps: true })

collectiviteSchema.plugin(aggregatePaginate);
module.exports = collectivity = mongoose.model('collectivite', collectiviteSchema);
