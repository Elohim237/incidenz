const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const responsableSchema = new mongoose.Schema({
    collectiviteId: [{ type: Schema.Types.ObjectId, ref: 'collectivite' }],
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    tel: {
        type: String,
        required: true,
    },

}, { timestamps: true })

responsableSchema.plugin(aggregatePaginate);
module.exports = responsable = mongoose.model('responsable', responsableSchema);
