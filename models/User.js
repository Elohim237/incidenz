const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
const { Schema } = require('mongoose');
const {integer} = require("twilio/lib/base/deserialize");

const UserSchema = new Schema({
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
    user_name: {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: false,
    },
    password: {
        type: String,
        required: false,
    },

    resetToken: {
        type: String,
    },
    resetTokenExpiration: {
        type: Date,
    },

    tel: {
        type: String,
        required: false,
    },
    role: {
        type: Number,
        required: true,
    },
    created_date: {
        type: Date,
        default: Date.now(),
        index: true,
    },

    avatar: {
        type: String,
        default: '',
    },


});

// Hash the password before saving
UserSchema.pre('save', function (next) {
    const user = this;
    if (!user.isModified('password')) return next();

    bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, (error, hash) => {
            if (err) return next(error);
            user.password = hash;
            next();
        });
    });
});

UserSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform(doc, ret) {
        // remove these props when object is serialized
        // eslint-disable-next-line no-param-reassign
        delete ret.password;
    },
});

// Compare password method
UserSchema.methods.comparePassword = (password, dbPassword, callback) => {
    bcrypt.compare(password, dbPassword, (err, isMatch) => {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};

UserSchema.plugin(aggregatePaginate);

const user = mongoose.model('user', UserSchema);
module.exports = user;
