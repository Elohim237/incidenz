const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const categorySchema = new Schema({
    collectiviteId: [{ type: Schema.Types.ObjectId, ref: 'collectivite' }],
    name: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    }
}, { timestamps: true })

categorySchema.plugin(aggregatePaginate);
module.exports = category = mongoose.model('category', categorySchema);
