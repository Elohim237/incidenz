
const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const usagerSchema = new Schema({

    username: {
        type: String,
        required: false,
    },

    created_date: {
        type: Date,
        default: Date.now(),
        index: true,
    },
}, { timestamps: true })

usagerSchema.plugin(aggregatePaginate);

const usager = mongoose.model('usager', usagerSchema);
module.exports = usager