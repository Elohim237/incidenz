const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const typeIncidentSchema = new Schema({
    categoryId: [{ type: Schema.Types.ObjectId, ref: 'category' }],
    responsableId: [{ type: Schema.Types.ObjectId, ref: 'responsable' }],
    name: {
        type: String,
        required: true,
    },
    created_date: {
        type: Date,
        default: Date.now(),
        index: true,
    },
}, { timestamps: true })

typeIncidentSchema.plugin(aggregatePaginate);
module.exports = typeIncident = mongoose.model('typeIncident', typeIncidentSchema);
