const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const incidentSchema = new Schema({
    collectiviteId: [{ type: Schema.Types.ObjectId, ref: 'collectivite' }],
    usagerId: [{type: Schema.Types.ObjectId, ref: 'usager'}],
    vocal: {
        type: String,
    },
    image: {
        type: String,
        required: true,
    },
    dagerousity: {
        type: Number,
        required: true,
    },
    desagreement: {
        type: Number,
        required: true,
    },
    etat: {
        type: Number,
        required: true,
        default: 0
    },
    position: {
        type: String,
        required: true,
    },
    canular: {
        type: String,
        required: false,
        default: "ISSUES"
    },

}, { timestamps: true })

incidentSchema.plugin(aggregatePaginate);
module.exports = incident = mongoose.model('incident', incidentSchema);
