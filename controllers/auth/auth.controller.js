const crypto = require('crypto');
const bcrypt = require('bcryptjs');
//const emailService = require('../../common/NewServiceEmailSender');
const User = require('../../models/User');

class AuthController {
  async forgotPassword(email, req) {
    try {
      const user = await User.findOne({ email });

      if (!user) {
        throw new Error('User not found');
      }

      console.log('User found', user);

      const resetToken = crypto.randomBytes(20).toString('hex');
      user.resetToken = resetToken;
      user.resetTokenExpiration = Date.now() + 3600000;
      await user.save();

     // await emailService.sendResetPasswordEmail(user.email, resetToken);
    } catch (error) {
      console.log('Error sending reset password email', error);
      throw error;
    }
  }

  async resetPassword(token, password) {
    try {
      const user = await User.findOne({
        resetToken: token,
        resetTokenExpiration: { $gt: Date.now() },
      });

      if (!user) {
        throw new Error('User not found');
      }

      user.password = password;
      user.resetToken = undefined;
      user.resetTokenExpiration = undefined;
      user.save((err) => {
        if (err) {
          throw new Error('Unable to update user password');
        }
      });

      console.log('Password reset successful for user with email: ', user.email);
    } catch (error) {
      console.log('Error resetting password', error);
      throw error;
    }
  }
}

const authController = new AuthController();

module.exports = authController;
