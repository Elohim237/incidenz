const base64Img = require('base64-img');
const path = require('path');
const fs = require('fs');

const uploadImage = (imageDir, req) => {

    const images = [];
    if (!fs.existsSync(imageDir)) {
        fs.mkdirSync(imageDir);
    }

    // Service Image Upload Code

    let image_type = req.body.image.split(';')[0].split('/')[1];
    let type = ['jpeg', 'png'];
    console.log(image_type)
    if ((image_type !== type[0]) && (image_type !== type[1])) {
        throw Error('Type is not valid')
    }

    let image_name = '';
    const possible = 'abcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 8; i++) {
        image_name += possible.charAt(
            Math.floor(Math.random() * possible.length),
        );
    }
    try {
         base64Img.img(
            req.body.image ,
            imageDir,
            image_name,
             (err, filepath) => {
                if (err) {
                    console.log('profile image upload error ', err);
                } else {
                    const resultImageName = `${image_name}.${image_type}`;

                }
            },
        );
         return `${image_name}.${image_type}`;
    } catch (error) {
       throw error;
    }
}
const generateRandomPassword = (length) =>{
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:'<>,.?/";
    let password = "";

    for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * charset.length);
        password += charset[randomIndex];
    }

    return password;
}

// Exemple d'utilisation pour générer un mot de passe de 12 caractères
const password = generateRandomPassword(12);
console.log(password);

module.exports = {
    generateRandomPassword,
    uploadImage
};