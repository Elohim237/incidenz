const express = require('express');
const router = express.Router();
const collectivityRoute = require('./collectivity.route');

router.use('/', collectivityRoute)

module.exports = router;

