const Joi = require('joi');
const express = require('express');
const router = express.Router();
const collectivityRepository = require('../../repositories/CollectiviteRepository');
const responsableRepository = require('../../repositories/ResponsableRepository');
const userRepository = require('../../repositories/UserRepository');
const Role = require("../../config/role");
const validateSchema = require('../../middlewares/validationSchema');
const {generateRandomPassword} = require("../../common/Helper");
const collectivitySchema = Joi.object({
    community: {
        name: Joi.string().required(),
        email: Joi.string().required(),
        tel: Joi.string().required(),
        address: Joi.string(),
        category: Joi.string()
    },
    manager: {
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().required(),
        tel: Joi.string().required(),
    }
})

router.post('/create', validateSchema(collectivitySchema), async (req, res) => {
    try {
        let collectivity = await collectivityRepository.save(req.body.community);
        let responsableData = {collectiviteId: collectivity._id, ...req.body.manager}
        let responsable = await responsableRepository.save(responsableData);
        let dataUser = {password: generateRandomPassword(10), ...req.body.manager, role: Role.COMPANY_ADMIN};
        let user = await userRepository.save(dataUser);

        return res.status(200).send(collectivity);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });
    }
})

router.get('/all-pagination/', async (req, res) => {
    try {
        const options = {
            page: parseInt(req.query.page) || 1,
            limit: req.query.limit || 10,
            search: req.query.search || '',
            sortDirection: req.query.order === 'asc' ? -1 : 1,
            sortBy: req.query.sort || 'createdAt',
        };

        console.log(options)

        const response = await collectivityRepository.getAllWithPagination(options);

        const pagination = {
            hasNextPage: response.hasNextPage,
            hasPrevPage: response.hasPrevPage,
            limit: response.limit,
            nextPage: response.nextPage,
            page: response.page - 1,
            pagingCounter: response.pagingCounter,
            prevPage: response.prevPage,
            totalDocs: response.totalDocs,
            totalPages: response.totalPages,
        };

        console.log(response)

        return res.status(200).json({
            collectivities: response.docs,
            pagination,
        });
    } catch (error) {
        console.log('Error in getting collectivities', error);
        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });
    }
})


module.exports = router;