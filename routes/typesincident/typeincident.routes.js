const express = require("express");
const typeIncidentRepository = require("../../repositories/TypeIncidentRepository");
const typeIncident = require("../../models/TypeIncident");
const router = express.Router();

router.get("/all", async (req, res) => {
    try {
        const allTypeIncidents = await typeIncidentRepository.getAllTypeIncidents();
        console.log('All incident types retrieved successfully:', allTypeIncidents);
        return res.status(200).json({allTypeIncidents});

    } catch (error){
        console.error('Error retrieving incident types:', error);

        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });

    }
});
router.post("/save", async (req, res) => {

    try {
        const newTypeIncident = await typeIncidentRepository.createTypeIncident(req.body);
        console.log('Incident type created successfully:', newTypeIncident);
        return res.status(200).json({newTypeIncident});

    } catch (error) {
        console.error('Error creating incident type:', error);
        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });
    }
});


router.post("/update/:id", async (req, res) => {
    try {
        const updatedTypeIncident = await typeIncidentRepository.updateTypeIncident(req.params, req.body);
        console.log('Incident type updated successfully:', updatedTypeIncident);
        return res.status(200).json({updatedTypeIncident});

    } catch (error) {
        console.error('Error updating incident type:', error);
        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });
    }
});
router.post("/delete/:id", async (req, res) => {
    try {
        const deletedTypeIncident = await typeIncidentRepository.deleteTypeIncident(req.params);
        console.log('Incident type deleted successfully:', deletedTypeIncident);
        return res.status(200).json({deletedTypeIncident});

    } catch (error) {
        console.error('Error deleting incident type:', error);
        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });
    }
});

router.get('/category/:id', async (req, res) => {
    try {
        const typeIncidentByCategory = await typeIncidentRepository.getByCategoryId(req.params);
        console.log('Incident type get successfully:', typeIncidentByCategory);
        return res.status(200).json({typeIncidentByCategory});

    } catch (error) {
        console.error('Error getting incident type:', error);
        return res.status(500).json({
            success: false,
            data: [],
            message: 'Something went to wrong.',
        });
    }
})

module.exports = router;