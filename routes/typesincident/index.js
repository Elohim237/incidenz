const express = require("express");

const router = express.Router();
const typeIncidentRouter = require('./typeincident.routes');

router.use('/', typeIncidentRouter);

module.exports = router;