const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const config = require('dotenv').config();
const authMiddleware = require('../middlewares/authenticate.middleware');
const User = require('../models/User');

const authController = require('../controllers/auth/auth.controller');
const validateSchema = require('../middlewares/validationSchema');

const router = express.Router();

const loginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  rememberMe: Joi.boolean().default(false),
});

const forgotPasswordSchema = Joi.object({
  email: Joi.string().email().required(),
});

const resetPasswordSchema = Joi.object({
  token: Joi.string().required(),
  password: Joi.string().required(),
});

const generateAccessToken = (user) => {
  const payload = { sub: user._id };
  return jwt.sign(payload, config.parsed.JWT_SECRET, { expiresIn: '1h' });
};

const generateRefreshToken = (user) => {
  const payload = { sub: user._id };
  return jwt.sign(payload, config.parsed.JWT_SECRET, { expiresIn: '7d' });
};

router.post('/login', validateSchema(loginSchema), async (req, res, next) => {
  try {
    const { email, password } = req.body;

    User.findOne({ email }, (err, user) => {
      if (err) {
        return res.status(500).json({ message: 'Internal server error', error: err });
      }

      if (!user) {
        return res.status(401).json({ message: 'Authentication failed. User not found.' });
      }

      user.comparePassword(password, user.password, (err, isMatch) => {
        if (err || !isMatch) {
          return res.status(401).json({ message: 'Authentication failed. Invalid password.' });
        }

        const accessToken = generateAccessToken(user);
        const refreshToken = generateRefreshToken(user);

        res.json({
          message: 'Authentication successful', accessToken, refreshToken, user,
        });
      });
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
});

router.post('/register', (req, res) => {
  const {
    email, password, first_name, last_name, role
  } = req.body;
  User.findOne({ email }, (err, user) => {
    if (user) {
      return res.status(401).json({ message: 'User already exist' });
    }
  } )
  const newUser = new User({
    email, password, first_name, last_name, role // first_name and last_name in database fields
  });

  newUser.save((err) => {
    console.log(err)
    if (err) {
      return res.status(400).json({ message: 'Registration failed', error: err });
    }

    res.json({ message: 'Registration successful' });
  });
});

router.get('/me', passport.authenticate('jwt', { session: false }), (req, res) => {
  res.json(req.user);
});

router.get('/meTest', authMiddleware.authenticate, (req, res) => {
  res.json(req.user);
});

router.post('/sign-in-with-token', passport.authenticate('jwt', { session: false }), (req, res) => {
  const accessToken = generateAccessToken(req.user);

  res.json({ message: 'Authentication with token successful', accessToken, user: req.user });
});

// Route to refresh the access token using the refresh token
router.post('/token/refresh', (req, res) => {
  const { refreshToken } = req.body;

  if (!refreshToken) {
    return res.status(400).json({ message: 'Refresh token not provided.' });
  }

  jwt.verify(refreshToken, config.parsed.JWT_SECRET, (err, payload) => {
    if (err) {
      return res.status(401).json({ message: 'Invalid refresh token.' });
    }

    User.findById(payload.sub, (userErr, user) => {
      if (userErr) {
        return res.status(500).json({ message: 'Internal server error', error: err });
      }

      if (!user) {
        return res.status(401).json({ message: 'Authentication failed. User not found.' });
      }

      const accessToken = generateAccessToken(user);
      res.json({ message: 'Access token refreshed successfully', accessToken });
    });
  });
});

router.post('/forgot-password', validateSchema(forgotPasswordSchema), (req, res) => {
  try {
    const { email } = req.body;

    authController.forgotPassword(email, req).then((result) => {
      res.json({ message: 'Email sent', result });
    }).catch((err) => {
      console.log('Error sending email', err);
      res.status(500).json({ error: 'Internal server error' });
    });
  } catch (err) {
    console.error(err);

    res.status(500).json({ error: 'Internal server error' });
  }
});

router.post('/reset-password', validateSchema(resetPasswordSchema), (req, res) => {
  try {
    const { token, password } = req.body;

    authController.resetPassword(token, password).then((result) => {
      res.json({ message: 'Password reset successful', result });
    }).catch((err) => {
      console.log('Error resetting password', err);
      res.status(500).json({ error: 'Internal server error' });
    });
  } catch (err) {
    console.error(err);

    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
