const express = require("express");
const router = express.Router();
const multer = require('multer');
const path = require('path');
const fs = require('fs').promises; // Utilisation de fs.promises pour une syntaxe asynchrone
const crypto = require('crypto');
const fileType = require('file-type');
const IncidentRepository = require('../../repositories/IncidentRepository');
const {uploadImage} = require("../../common/Helper");

const storage = multer.diskStorage({
    destination: async (req, file, cb) => {
        const destinationFolder = path.join(__dirname, '../../public/incidents/');
        console.log(destinationFolder);

        try {
            await fs.mkdir(destinationFolder, { recursive: true });
            cb(null, destinationFolder);
        } catch (error) {
            console.error('Erreur lors de la création du dossier de destination :', error);
            cb(error);
        }
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + ".jpg")
    }
});

const upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 },
    fileFilter: (req, file, cb) => {
        const allowedMimes = ['image/jpeg', 'image/png', 'image/gif'];
        console.log(allowedMimes);
        if (allowedMimes.includes(file.mimetype)) {
            cb(null, true);
        } else {
            const error = new Error('Type de fichier non autorisé.');
            console.error('Erreur lors de la vérification du type de fichier :', error);
            cb(error);
        }
    }
}).single('image');

router.post('/create', async (req, res) => {
    console.log(req);

   /* upload(req, res, async (error) => {
        if (error) {
            return res.status(400).send(error);
        } else {
            try {
                console.log(req);
                const id = req.body.usagerId;
                req.body.image = req.file.filename;
                const response = await IncidentRepository.save(req.body, id);
                return res.status(200).send(response);
            } catch (e) {
                // En cas d'erreur lors de l'enregistrement en base de données, supprimez le fichier téléchargé
                await fs.unlink(path.join(__dirname, '../../public/incidents/', req.file.filename));
                console.error('Erreur lors de l\'enregistrement en base de données:', e);
                res.status(500).send('Erreur lors de l\'enregistrement en base de données.');
            }
        }
    });*/
    const imageName = uploadImage('public/incidents', req)
    if (!imageName) {
        return res.status(400).send(error);
    } else {
        try {
            //console.log(req);
            const id = req.body.usagerId;
            req.body.image = imageName;
            const response = await IncidentRepository.save(req.body, id);
            return res.status(200).send(response);
        } catch (e) {
            // En cas d'erreur lors de l'enregistrement en base de données, supprimez le fichier téléchargé
            //await fs.unlink(path.join(__dirname, '../../public/incidents/', imageName));
            console.error('Erreur lors de l\'enregistrement en base de données:', e);
            res.status(500).send('Erreur lors de l\'enregistrement en base de données.');
        }
    }
});

module.exports = router;
