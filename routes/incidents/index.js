const express = require("express");

const router = express.Router();
const IncidentsRouter = require('./incident.routes');

router.use('/', IncidentsRouter);

module.exports = router;