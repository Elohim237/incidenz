const express = require("express");
const router = express.Router();
const Category = require('../../repositories/CategoryRository');
router.get('/all', async (req, res) => {
    try {
        const result = await Category.list();
        res.status(200).send(result);
    } catch (error) {
        res.status(400).send()
    }
})

module.exports = router;