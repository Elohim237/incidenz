const express = require('express');
const router = express.Router();
const categoryRoute = require('./category.routes');

router.use('/', categoryRoute)

module.exports = router;

