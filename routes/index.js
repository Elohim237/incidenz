const express = require('express');
const router = express.Router();
const collectivityRoute = require('./collectivite');
const typeIncident = require('./typesincident');
const auth = require('./auth.route');
const category = require('./categories');
const incident = require('./incidents');

router.use('/collectivity', collectivityRoute);
router.use('/typeIncident', typeIncident);
router.use('/auth', auth);
router.use('/category', category);
router.use('/incident', incident);
module.exports = router;
