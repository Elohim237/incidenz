module.exports = {
  apps: [{
    name: 'incidenz_backend',
    script: './bin/www',
    node_args: '-r dotenv/config',
    watch: true,
    env: { NODE_ENV: 'development' },
    env_production: { NODE_ENV: 'production' },
  }],
};
